import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

@Injectable()
export class DisplayService {

  subject  = new Rx.BehaviorSubject<string>("Загрузка");
  _msgs = Array<Message>();
  _lastCall: Date = new Date();

  constructor() { 
    
    Rx.Observable.timer(50, 50).subscribe(() => {
      if(this._msgs.length > 0) {

        var now = new Date();
        var timeRange = ((+now) - (+this._lastCall));
        if(timeRange >= this._msgs[0].timeout){
          this._lastCall = now;          
          var msg = this._msgs.shift();
          this.subject.next(msg.message);
        }
      }
    });
  }

  publishFast(msg: string) {
    var m = new Message(msg, 500);
    this._msgs.splice(0, this._msgs.length);
    this._msgs.push(m);
  }
  
  publishSlow(msg: string) { 
    var m = new Message(msg, 2500);
    this._msgs.push(m);
  }

}

class Message {
  constructor(message: string, timeoutInSec: number){
    this.message = message;
    this.timeout = timeoutInSec;
  }

   message: string;
   timeout: number;
}
