import { Component, OnInit, OnDestroy } from '@angular/core';
import { DisplayService } from '../display.service';
import { Subscription } from 'rxjs';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit, OnDestroy {

  private _msgSubsc: Subscription;
  text: string;

  constructor(private _displayService : DisplayService) {
    
   }

  ngOnInit() {


    this._msgSubsc = this._displayService.subject.subscribe(msg => {
      this.text = msg;
    });
  }

  ngOnDestroy(): void {
    this._msgSubsc.unsubscribe();
  }
}
