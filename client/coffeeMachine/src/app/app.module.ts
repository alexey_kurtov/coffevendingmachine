import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';


import { AppComponent } from './app.component';
import { CoffeeTypeSelectorComponent } from './coffee-type-selector/coffee-type-selector.component';
import { CoffeeMachineComponent } from './coffee-machine/coffee-machine.component';
import { DisplayComponent } from './display/display.component';
import { DisplayService } from './display.service';
import { DomainService } from './domain.service';
import { CoffeeItemComponent } from './coffee-item/coffee-item.component';
import { VolumeItemComponent } from './volume-item/volume-item.component';
import { PriceItemComponent } from './price-item/price-item.component';
import { CancelItemComponent } from './cancel-item/cancel-item.component';
import { StatisticComponent } from './statistic/statistic.component';


@NgModule({
  declarations: [
    AppComponent,
    CoffeeTypeSelectorComponent,
    CoffeeMachineComponent,
    DisplayComponent,
    CoffeeItemComponent,
    VolumeItemComponent,
    PriceItemComponent,
    CancelItemComponent,
    StatisticComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [DisplayService, DomainService],
  bootstrap: [AppComponent]
})
export class AppModule { }
