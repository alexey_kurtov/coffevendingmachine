import { Component, OnInit, ViewEncapsulation, Input  } from '@angular/core';
import { DrinkType, DomainService } from '../domain.service';
import { State } from '../state.enum';

@Component({
  selector: 'app-coffee-item',
  templateUrl: './coffee-item.component.html',
  styleUrls: ['./coffee-item.component.css'],
  host: {
    "(click)": "onClick($event)"
 }
})
export class CoffeeItemComponent {

  @Input() type: DrinkType;

  constructor(private _domainService : DomainService) { }

  onClick($event: any){
    
    if(this._domainService.currentState == State.Idle){
        this._domainService.setDrinkType(this.type);
    }
  }

}
