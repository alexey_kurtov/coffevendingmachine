import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DisplayService } from './display.service';
import { State } from './state.enum';
import * as Rx from 'rxjs';

@Injectable()
export class DomainService {

  currentState: State;
  
  constructor(private _http: HttpClient, private _displayService: DisplayService) {
    
  }

  reset(){
    this._http.get<DomainResponse>(this.getApiUrl("Reset")).subscribe(resp => {
      
        this.processResponse(resp);
    });
  }

  setDrinkType(type: DrinkType){
    this._http.get<DomainResponse>(this.getApiUrl("DrinkType/" + type)).subscribe(resp => {
      this.processResponse(resp);
    });
  }
  setDrinkVolume(volume: number){
    this._http.get<DomainResponse>(this.getApiUrl("DrinkVolume/" + volume)).subscribe(resp => {
      this.processResponse(resp);
    });
  }

  pushMoney(bill: number){
    this._http.get<DomainResponse>(this.getApiUrl("PushMoney/" + bill)).subscribe(resp => {
      this.processResponse(resp);
    });
  }

  cancel(){
    this._http.get<DomainResponse>(this.getApiUrl("Cancel")).subscribe(resp => {
      this.processResponse(resp);
    });
  }


  getStatisticObserver(): Rx.Observable<StatisticResponse> {
    return this._http.get<StatisticResponse>(this.getApiUrl("Statistic"));
  }

  private getApiUrl(subPath: string){
    return "http://localhost:65014/api/" + subPath;
  }


  private processResponse(resp: DomainResponse) {
    this.currentState = resp.currenState;
      if(resp && resp.messages) {
      resp.messages.forEach((msg, index) => {
        if(index < 1){
          console.log("fast");
          this._displayService.publishFast(msg);  
        }
        else {
          console.log("slow");
          this._displayService.publishSlow(msg);
        }
        
      });
    }
  }

}

export enum DrinkType {
  espresso = 0,
  moka = 1,
  americano = 2,
  ristretto = 3,
}

export class DomainResponse
{
    currenState : State;
    messages: string[];
}

export class StatisticResponse
{
    currentState : string;
}