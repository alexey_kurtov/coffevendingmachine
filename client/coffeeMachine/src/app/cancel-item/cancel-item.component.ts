import { Component } from '@angular/core';
import { DrinkType, DomainService } from '../domain.service';
import { State } from '../state.enum';

@Component({
  selector: 'app-cancel-item',
  templateUrl: './cancel-item.component.html',
  styleUrls: ['./cancel-item.component.css'],
  host: {
    "(click)": "onClick($event)"
  }
})
export class CancelItemComponent {

  constructor(private _domainService : DomainService) { }

  onClick($event: any) {
    
    if(this._domainService.currentState == State.SelectCoffeVolume || this._domainService.currentState == State.BillWating){
        this._domainService.cancel();
    }
  }
}
