import { Component, OnInit } from '@angular/core';
import { DomainService } from '../domain.service';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  state: string;

  constructor(private _domainService : DomainService) { }

  ngOnInit() {

    Rx.Observable.timer(1000, 1000).subscribe(() => {
      var subscr = this._domainService.getStatisticObserver().subscribe(resp =>{
          this.state = resp.currentState.trim();
        },
        () => subscr.unsubscribe(),
        () => subscr.unsubscribe()
      )

    });

  }

}
