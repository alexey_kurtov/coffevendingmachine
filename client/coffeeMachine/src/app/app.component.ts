import { Component } from '@angular/core';
import { DomainService } from './domain.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private _domainService: DomainService){
    _domainService.reset();
  }
}
