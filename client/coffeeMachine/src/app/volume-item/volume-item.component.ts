import { Component, Input  } from '@angular/core';
import { DrinkType, DomainService } from '../domain.service';
import { State } from '../state.enum';

@Component({
  selector: 'app-volume-item',
  templateUrl: './volume-item.component.html',
  styleUrls: ['./volume-item.component.css'],
  host: {
    "(click)": "onClick($event)"
 }
})
export class VolumeItemComponent {


  @Input() volume: number;
  
  constructor(private _domainService : DomainService) { }
  

  onClick($event: any){
    
    if(this._domainService.currentState == State.SelectCoffeVolume){
        this._domainService.setDrinkVolume(this.volume);
    }
  }

}
