import { Component, Input } from '@angular/core';
import { DrinkType, DomainService } from '../domain.service';
import { State } from '../state.enum';

@Component({
  selector: 'app-price-item',
  templateUrl: './price-item.component.html',
  styleUrls: ['./price-item.component.css'],
  host: {
    "(click)": "onClick($event)"
  }
})
export class PriceItemComponent {

  @Input() bill: number;
  
  constructor(private _domainService : DomainService) { }
  
  
    onClick($event: any) {
      
      if(this._domainService.currentState == State.BillWating){
          this._domainService.pushMoney(this.bill);
      }
    }

}
