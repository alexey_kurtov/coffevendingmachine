﻿using System;
using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain
{
    public interface ICoffeeMachine
    {
        void SelectDrink(DrinkType drinkType);
        void SetDrinkVolume(int volume);
        void PushMoney(Decimal value);
        void CancelOperation();
        State GetCurrentState();
        String[] GetLastMessages();
        void Reset();
    }
}