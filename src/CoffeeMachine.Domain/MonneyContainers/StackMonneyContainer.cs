﻿using System;

namespace CoffeeMachine.Domain.MonneyContainers
{
    public class StackMonneyContainer : IMonneyContainer
    {
        private readonly int _containerSize;
        private int _currentLevel;

        public StackMonneyContainer(decimal denomination, int containerSize)
        {
            _containerSize = containerSize;
            _currentLevel = 0;
            Denomination = denomination;
        }

        public decimal Denomination { get; }
        public bool CanPopMoney => _currentLevel > 0;
        public bool CanPushMoney => _currentLevel < _containerSize;
        public int Level => _currentLevel;

        public void Pop()
        {
            if (!CanPopMoney)
            {
                throw new Exception("Impossible to return money");
            }
            _currentLevel--;
        }

        public void Push()
        {
            if (!CanPushMoney)
            {
                throw new Exception("Impossible to receive money");
            }
            
            _currentLevel++;
        }

        public override string ToString()
        {
            return $"[ПРИЕМНИК ДЕНЕГ] Номинал: '{Denomination:C}', Текущее кол-во: '{Level}',  Максимально: '{_containerSize}'";
        }
    }
}