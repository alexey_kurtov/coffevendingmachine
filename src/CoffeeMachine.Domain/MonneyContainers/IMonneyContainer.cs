﻿using System.Collections.Generic;

namespace CoffeeMachine.Domain.MonneyContainers
{
    public interface IMonneyContainer
    {
        decimal Denomination { get; }
        bool CanPopMoney { get; }
        bool CanPushMoney { get; }
        int Level { get; }
        void Pop();
        void Push();
    }
}