﻿namespace CoffeeMachine.Domain.ComponentContainers
{
    public class ComponetValidationResult
    {
        public ComponetValidationResult(Trigger supposedTrigger, bool isValid = true, string reason = null, ushort stateRank = 0)
        {
            IsValid = isValid;
            Reason = reason;
            SupposedTrigger = supposedTrigger;
            StateRank = stateRank;
        }

        public bool IsValid { get; }
        public string Reason { get; }
        public Trigger SupposedTrigger { get; }
        public ushort StateRank { get; }
    }
}