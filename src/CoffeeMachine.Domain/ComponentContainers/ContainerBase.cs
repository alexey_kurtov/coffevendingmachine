using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public abstract class ContainerBase : IDrinkComponentContainer
    {
        protected int CommponetCount;

        protected ContainerBase(string componentName, int commponetCount)
        {
            Name = componentName;
            CommponetCount = commponetCount;
        }

        public string Name { get; }
        public abstract ComponetValidationResult IsAvailable(IDrink drink);

        public abstract void GetComponetFor(IDrink drink);
        public virtual bool IsSupported(IDrink drink)
        {
            return true;
        }

        public override string ToString()
        {
            return $"[���������] ���������: '{Name}' ���������: '{CommponetCount}' ";
        }
    }
}