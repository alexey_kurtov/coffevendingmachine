﻿using System.Linq;
using System.Text;
using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public class AllComponentContainer : IDrinkComponentContainer
    {
        private readonly IDrinkComponentContainer[] _containers;

        public AllComponentContainer(params IDrinkComponentContainer[] containers)
        {
            _containers = containers;
        }

        public string Name => "Все компоненты для приготовления напитков";
        public ComponetValidationResult IsAvailable(IDrink drink)
        {
            var containers = _containers.Where(c => c.IsSupported(drink)).ToArray();
            if (containers.Any())
            {
                var allResults = containers.Select(c => c.IsAvailable(drink)).OrderBy(r => r.IsValid).ThenByDescending(r => r.StateRank);
                var result = allResults.FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
            }

            return new ComponetValidationResult(Trigger.SystemErrorOccurred, false, $"Произошла ошибка системы компонентов.", 99);
        }

        public void GetComponetFor(IDrink drink)
        {
            var cupContainers = _containers.Where(c => c.IsSupported(drink)).ToArray();

            foreach (var cupContainer in cupContainers)
            {
                cupContainer.GetComponetFor(drink);
            }
        }

        public bool IsSupported(IDrink drink)
        {
            return _containers.Any(c => c.IsSupported(drink));
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var container in _containers)
            {
                builder.AppendLine(container.ToString());
            }
            return builder.ToString();
        }
    }
}