﻿using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public interface IDrinkComponentContainer
    {
        string Name { get; }
        ComponetValidationResult IsAvailable(IDrink drink);
        void GetComponetFor(IDrink drink);
        bool IsSupported(IDrink drink);
    }
}