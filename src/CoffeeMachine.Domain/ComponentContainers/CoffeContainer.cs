using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public class CoffeContainer : ContainerBase
    {
        public CoffeContainer(int currentCoffeePortion) : base("����", currentCoffeePortion){}


        public override ComponetValidationResult IsAvailable(IDrink drink)
        {
            if (CommponetCount <= 0)
            {
                return new ComponetValidationResult(Trigger.CoffeeRanOut, false, "��� ���� � ��������", 9);
            }

            var hasCoffee = CommponetCount >= drink.CoffeePortion;

            if (!hasCoffee)
            {
                return new ComponetValidationResult(Trigger.CoffeeNotEnough, false, "������������ ���� ��� ������������ �������. �������� ������ �������");
            }

            return new ComponetValidationResult(Trigger.ComponentsConfirmed);
        }

        public override void GetComponetFor(IDrink drink)
        {
            CommponetCount = CommponetCount - drink.CoffeePortion;
        }
    }
}