using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public class CupContainer : ContainerBase
    {
        private readonly int _cupVolume;
        public CupContainer(int cupVolume, int cupCount) : base($"���� {cupVolume}", cupCount)
        {
            _cupVolume = cupVolume;
        }

        public override ComponetValidationResult IsAvailable(IDrink drink)
        {

            if (CommponetCount <= 0)
            {
                return new ComponetValidationResult(Trigger.CupNotEnough, false, $"��� ���� ������� '{_cupVolume}'. �������� ������ �����");
            }


            return new ComponetValidationResult(Trigger.ComponentsConfirmed);
        }

        public override void GetComponetFor(IDrink drink)
        {
            CommponetCount = CommponetCount - 1;
        }

        public override bool IsSupported(IDrink drink)
        {
            return drink.Volume == _cupVolume;
        }

        public bool IsEmpty()
        {
            return CommponetCount <= 0;
        }
    }
}