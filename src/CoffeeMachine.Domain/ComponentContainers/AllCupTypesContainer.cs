﻿using System.Linq;
using System.Text;
using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public class AllCupTypesContainer : IDrinkComponentContainer
    {
        private readonly CupContainer[] _containers;

        public AllCupTypesContainer(params CupContainer[] containers)
        {
            _containers = containers;
        }

        public string Name => "Лоток с тарой";
        public ComponetValidationResult IsAvailable(IDrink drink)
        {

            if (_containers.All(c => c.IsEmpty()))
            {
                return new ComponetValidationResult(Trigger.CupRanOut, false, $"Нет тары в лотках", 9);
            }

            var cupContainers = _containers.Where(c => c.IsSupported(drink)).ToArray();
            if (cupContainers.Any())
            {
                var result = cupContainers.Select(c => c.IsAvailable(drink)).OrderBy(c => c.IsValid).ThenByDescending(r => r.StateRank).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
            }

            return new ComponetValidationResult(Trigger.CupRanOut, false, $"Произошла системная ошибка", 9);
        }

        public void GetComponetFor(IDrink drink)
        {
            var cupContainers = _containers.Where(c => c.IsSupported(drink)).ToArray();

            foreach (var cupContainer in cupContainers)
            {
                cupContainer.GetComponetFor(drink);
            }
        }

        public bool IsSupported(IDrink drink)
        {
            return true;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var container in _containers)
            {
                builder.AppendLine(container.ToString());
            }
            return builder.ToString();
        }
    }
}