using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.ComponentContainers
{
    public class WaterContainer : ContainerBase
    {
        private const int MinWateVolume = 150;

        public WaterContainer(int currentWaterCount) : base("����", currentWaterCount) {}

        public override ComponetValidationResult IsAvailable(IDrink drink)
        {
            if (CommponetCount <= MinWateVolume)
            {
                return new ComponetValidationResult(Trigger.WaterRanOut ,false, "��� ���� � ��������", 9);
            }

            var hasWater = CommponetCount >= drink.Volume;
            if (!hasWater)
            {
                return new ComponetValidationResult(Trigger.WaterNotEnough, false, "������������ ���� ��� ������������ �������");
            }

            return  new ComponetValidationResult(Trigger.ComponentsConfirmed);
        }

        public override void GetComponetFor(IDrink drink)
        {
            CommponetCount = CommponetCount - drink.Volume;
        }
    }
}