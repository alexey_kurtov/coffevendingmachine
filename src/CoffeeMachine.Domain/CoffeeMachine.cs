﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoffeeMachine.Domain.ComponentContainers;
using CoffeeMachine.Domain.Drinks;
using CoffeeMachine.Domain.Helpers;
using CoffeeMachine.Domain.Payments;
using Stateless;

namespace CoffeeMachine.Domain
{
    public class CoffeeMachine : ICoffeeMachine
    {
        private readonly IDrinkFacrory _drinkFacrory;
        private readonly IAccountManagerFactory _accountManagerFactory;
        private readonly IDrinkComponentContainer _componentContainer;
        private IAccountManager _accountManager;

        private State _currentState =  State.Idle;
        private StateMachine<State, Trigger> _makeCoffee;

        private IDrink _drink;
        private readonly List<string> _msg;
        private int _lastMessagesPossition;

        public CoffeeMachine(IDrinkFacrory drinkFacrory, IAccountManagerFactory accountManagerFactory, IDrinkComponentContainer componentContainer)
        {
            _drinkFacrory = drinkFacrory;
            _accountManagerFactory = accountManagerFactory;
            _componentContainer = componentContainer;
            _msg = new List<string>();
            initializeStateMachine();

            //string graph = UmlDotGraph.Format(_makeCoffee.GetInfo());
        }

        private void initializeStateMachine()
        {
            _msg.Clear();
            _msg.Add("Выберите напиток");
            _lastMessagesPossition = 0;
            _currentState = State.Idle;
            _makeCoffee = new StateMachine<State, Trigger>(() => _currentState, state => _currentState = state);


            _makeCoffee.Configure(State.Idle)
                .OnEntry(() => { _msg.Add("Выберите напиток"); })
                .Permit(Trigger.DrinkTypeSelected, State.SelectCoffeVolume);


            _makeCoffee.Configure(State.SelectCoffeVolume)
                .OnEntry(() => _msg.Add("Выберите объем напитка"))
                .Permit(Trigger.OperationCanceled, State.Idle)
                .PermitReentry(Trigger.WaterNotEnough)
                .PermitReentry(Trigger.CupNotEnough)
                .Permit(Trigger.CoffeeNotEnough, State.Idle)
                .Permit(Trigger.WaterRanOut, State.NeedService)
                .Permit(Trigger.CoffeeRanOut, State.NeedService)
                .Permit(Trigger.CupRanOut, State.NeedService)
                .Permit(Trigger.ComponentsConfirmed, State.MoneyOperation);

            _makeCoffee.Configure(State.MoneyOperation)
                .OnEntry(() =>
                {
                    _accountManager = _accountManagerFactory.Create(_drink);
                    _makeCoffee.Fire(Trigger.AccountManagerInitialized);
                })
                .Permit(Trigger.AccountManagerInitialized, State.BillWating)
                .Permit(Trigger.OperationCanceled, State.ReturnAllMonney);

            _makeCoffee.Configure(State.BillWating)
                .SubstateOf(State.MoneyOperation)
                .OnEntry(waitBill)
                .Permit(Trigger.MoneyPushed, State.CheckAccount)
                .PermitReentry(Trigger.LastBillReturned);

            _makeCoffee.Configure(State.CheckAccount)
                .SubstateOf(State.MoneyOperation)
                .OnEntry(validateAccount)
                .Permit(Trigger.AccountVerificationPassed, State.ReturnRestOfMonney)
                .Permit(Trigger.AccountVerificationFailed, State.BillWating);

            _makeCoffee.Configure(State.ReturnAllMonney)
                .SubstateOf(State.MoneyOperation)
                .OnEntry(returnAllMoney)
                .Permit(Trigger.AllMoneyReturned, State.Idle)
                .Permit(Trigger.SystemErrorOccurred, State.NeedService)
                .Permit(Trigger.AccountVerificationFailed, State.BillWating);

            _makeCoffee.Configure(State.ReturnRestOfMonney)
                .SubstateOf(State.MoneyOperation)
                .OnEntry(tryMakePayment)
                .Permit(Trigger.PaymentWasMade, State.MakeDrink)
                .Permit(Trigger.ImpossibleToReturnMoneyDifference, State.ReturnAllMonney);

            _makeCoffee.Configure(State.MakeDrink)
                .OnEntry(makeCoffe)
                .Permit(Trigger.DrinkMade, State.Idle);

            _makeCoffee.Configure(State.NeedService)
                .OnEntry(() => _msg.Add("Необходим сервис. Обратитесь в службу поддержки"));
        }

        private void makeCoffe()
        {
            _componentContainer.GetComponetFor(_drink);
            _msg.Add($"Идет изготовление напитка '{Enum.GetName(typeof(DrinkType), _drink.DrinkType)}'");
            _msg.Add($"Напиток готов... Приятного аппетита!");
            _makeCoffee.Fire(Trigger.DrinkMade);
        }

        private void tryMakePayment()
        {
            if (_accountManager.HasMoneyChange)
            {
                try
                {
                    if (_accountManager.TryReturnMoneyChange(out var change))
                    {
                        _msg.Add($"Заберите сдачу '{change:C}'");
                        _makeCoffee.Fire(Trigger.PaymentWasMade);
                    }
                }
                catch (MoneyOperationException ex)
                {
                    _msg.Add(ex.Reason);
                    _makeCoffee.Fire(ex.SupposedTrigger);
                }
            }
            else
            {
                _makeCoffee.Fire(Trigger.PaymentWasMade);
            }
            
        }
        private void waitBill()
        {
            _msg.Add($"Внесите сумму '{_drink.Amount:C}' для оплаты напитка. Внесено ''{_accountManager.Total:C}''");
        }

        private void returnAllMoney()
        {
            try
            {
                var total = _accountManager.Total;
                if (total <= 0)
                {
                    _makeCoffee.Fire(Trigger.AllMoneyReturned);
                    return;
                }
                if (_accountManager.TryReturnAllMoney())
                {
                    _msg.Add($"Возврат внесенных денег - '{total:C}'");
                    _makeCoffee.Fire(Trigger.AllMoneyReturned);
                }
            }
            catch (MoneyOperationException ex)
            {
                _msg.Add(ex.Reason);
                _makeCoffee.Fire(ex.SupposedTrigger);
            }

        }

        private void validateAccount()
        {
            if (_accountManager.IsAccountValid)
            {
                _makeCoffee.Fire(Trigger.AccountVerificationPassed);
            }
            else
            {
                _makeCoffee.Fire(Trigger.AccountVerificationFailed);
            }

        }

        private void validateCoffeComponent()
        {
            var result = _componentContainer.IsAvailable(_drink);

            if (result.IsValid)
            {
                _makeCoffee.Fire(Trigger.ComponentsConfirmed);
            }
            else
            {
                _msg.Add( result.Reason);
                _makeCoffee.Fire(result.SupposedTrigger);
            }
        }

        public void SelectDrink(DrinkType drinkType)
        {
            _drink = _drinkFacrory.Create(drinkType);
            _makeCoffee.Fire(Trigger.DrinkTypeSelected);
        }

        public void SetDrinkVolume(int volume)
        {
            _drink.Volume = volume;
            validateCoffeComponent();
        }

        public void PushMoney(Decimal value)
        {
            try
            {
                _accountManager.MakePayment(value);
                _makeCoffee.Fire(Trigger.MoneyPushed);
            }
            catch (MoneyOperationException ex)
            {
                _msg.Add(ex.Reason);
                _makeCoffee.Fire(ex.SupposedTrigger);
            }
        }


        public void CancelOperation()
        {
            _msg.Add("Операция отменена пользователем");
            _makeCoffee.Fire(Trigger.OperationCanceled);
        }

        public State GetCurrentState()
        {
            return _currentState;
        }

        public string[] GetLastMessages()
        {
            var messages = _msg.Skip(_lastMessagesPossition).ToArray();
            _lastMessagesPossition = _msg.Count;
            return messages;
        }

        public void Reset()
        {
            initializeStateMachine();
        }
    }
}

