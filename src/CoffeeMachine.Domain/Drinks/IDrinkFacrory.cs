﻿namespace CoffeeMachine.Domain.Drinks
{
    public interface IDrinkFacrory
    {
        IDrink Create(DrinkType drinkType);
    }
}