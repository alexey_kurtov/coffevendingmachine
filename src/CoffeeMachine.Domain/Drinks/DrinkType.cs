﻿namespace CoffeeMachine.Domain.Drinks
{
    public enum DrinkType
    {
        Espresso = 0,
        Moka = 1,
        Americano = 2,
        Ristretto = 3,
    }
}