﻿namespace CoffeeMachine.Domain.Drinks
{
    public interface IDrink {
        DrinkType DrinkType { get; }
        int Volume { get; set; }
        int CoffeePortion { get; }

        decimal Amount { get; }
    }
}