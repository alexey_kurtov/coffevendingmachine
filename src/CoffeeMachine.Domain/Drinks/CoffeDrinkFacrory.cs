﻿using System;
using CoffeeMachine.Domain.Prices;

namespace CoffeeMachine.Domain.Drinks
{
    public class CoffeDrinkFacrory : IDrinkFacrory
    {
        private readonly IDrinkPriceStrategy _priceStrategy;

        public CoffeDrinkFacrory(IDrinkPriceStrategy priceStrategy)
        {
            _priceStrategy = priceStrategy;
        }

        public IDrink Create(DrinkType drinkType)
        {
            switch (drinkType)
            {
                case DrinkType.Americano:
                    return new CoffeeDrink(drinkType, 1, _priceStrategy);
                case DrinkType.Espresso:
                    return new CoffeeDrink(drinkType, 2, _priceStrategy);
                case DrinkType.Moka:
                    return new CoffeeDrink(drinkType, 1, _priceStrategy, 200);
                case DrinkType.Ristretto:
                    return new CoffeeDrink(drinkType, 2, _priceStrategy, 400);
            }

            throw new Exception($"Drink type '{Enum.GetName(typeof(DrinkType), drinkType)}' not supported");
        }
    }
}