using CoffeeMachine.Domain.Prices;

namespace CoffeeMachine.Domain.Drinks
{
    public class CoffeeDrink : IDrink
    {
        private readonly IDrinkPriceStrategy _priceStrategy;

        public CoffeeDrink(DrinkType drinkType, int coffeePortion, IDrinkPriceStrategy priceStrategy, int defaultVolume = 150)
        {
            _priceStrategy = priceStrategy;
            DrinkType = drinkType;
            Volume = defaultVolume;
            CoffeePortion = coffeePortion;
        }

        public DrinkType DrinkType { get; }
        public int Volume { get; set; }
        public int CoffeePortion { get; }
        public decimal Amount => _priceStrategy.GetAmount(this);
    }
}