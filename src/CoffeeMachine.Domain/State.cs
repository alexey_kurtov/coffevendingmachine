﻿namespace CoffeeMachine.Domain
{
    public enum State
    {
        Idle = 0,
        SelectCoffeVolume = 1,

        MoneyOperation = 2,
        BillWating = 3,
        CheckAccount = 4,

        ReturnAllMonney = 5,
        ReturnRestOfMonney = 6,

        NeedService = 7,
        MakeDrink = 8
    }
}