﻿using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.Prices
{
    public interface IDrinkPriceStrategy
    {
        decimal GetAmount(IDrink drink);
    }
}