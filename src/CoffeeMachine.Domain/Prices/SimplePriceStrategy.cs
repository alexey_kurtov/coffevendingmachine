﻿using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.Prices
{
    public class SimplePriceStrategy : IDrinkPriceStrategy
    {
        public decimal GetAmount(IDrink drink)
        {
            return drink.CoffeePortion * 50 + drink.Volume / 100 * 2;
        }
    }
}