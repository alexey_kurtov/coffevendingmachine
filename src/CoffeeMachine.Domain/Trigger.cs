﻿namespace CoffeeMachine.Domain
{
    public enum Trigger
    {
        DrinkTypeSelected,
        WaterRanOut,
        WaterNotEnough,
        CoffeeRanOut,
        CoffeeNotEnough,
        CupNotEnough,
        CupRanOut,
        ComponentsConfirmed,
        AccountManagerInitialized,
        OperationCanceled,
        SystemErrorOccurred,
        MoneyPushed,
        AccountVerificationFailed,
        AccountVerificationPassed,
        PaymentWasMade,
        AllMoneyReturned,
        ImpossibleToReturnMoneyDifference,
        DrinkMade,
        LastBillReturned
    }
}