﻿using System;

namespace CoffeeMachine.Domain.Helpers
{
    public class MoneyOperationException : Exception
    {
        public MoneyOperationException(Trigger supposedTrigger, string reason) : base(reason)
        {
            Reason = reason;
            SupposedTrigger = supposedTrigger;
        }

        public string Reason { get; }
        public Trigger SupposedTrigger { get; }
    }
}