﻿using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.Payments
{
    public interface IAccountManagerFactory
    {
        IAccountManager Create(IDrink drink);
    }
}