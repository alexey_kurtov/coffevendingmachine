﻿namespace CoffeeMachine.Domain.Payments
{
    public interface IAccountManager
    {
        decimal Total { get; }
        void MakePayment(decimal value);
        bool TryReturnMoneyChange(out decimal сhange);
        bool HasMoneyChange { get; }
        bool IsAccountValid { get; }
        bool TryReturnAllMoney();
    }
}