﻿using CoffeeMachine.Domain.Drinks;
using CoffeeMachine.Domain.MonneyContainers;
using CoffeeMachine.Domain.Payments.ReturnMoneyStrategy;

namespace CoffeeMachine.Domain.Payments
{
    public class AccountManagerFactory : IAccountManagerFactory
    {
        private readonly IMonneyContainer[] _containers;

        public AccountManagerFactory(params IMonneyContainer[] containers)
        {
            _containers = containers;
        }

        public IAccountManager Create(IDrink drink)
        {
            return new CurrencyDetectorAccountManager(drink, new SimpleMoneyChangeStrategy(_containers), _containers);
        }
    }
}