using System;
using System.Collections.Generic;
using System.Linq;
using CoffeeMachine.Domain.Drinks;
using CoffeeMachine.Domain.Helpers;
using CoffeeMachine.Domain.MonneyContainers;
using CoffeeMachine.Domain.Payments.ReturnMoneyStrategy;

namespace CoffeeMachine.Domain.Payments
{
    public class CurrencyDetectorAccountManager : IAccountManager
    {
        private readonly IDrink _drink;
        private readonly IReturnMoneyChangeStrategy _strategy;
        private readonly IMonneyContainer[] _containers;
        private readonly Stack<decimal> _bills;

        public CurrencyDetectorAccountManager(IDrink drink, IReturnMoneyChangeStrategy strategy, params IMonneyContainer[] containers)
        {
            _drink = drink;
            _strategy = strategy;
            _bills = new Stack<decimal>();
            _containers = containers;
        }

        public decimal Total => _bills.Sum(bill => bill);

        public void MakePayment(decimal value)
        {
            if (_containers.All(c => !c.CanPushMoney))
            {
                throw new MoneyOperationException(Trigger.SystemErrorOccurred, $"���������� ������ ������ �� ����");
            }

            var container = _containers.FirstOrDefault(c => c.Denomination == value);
            if (container == null)
            {
                throw new MoneyOperationException(Trigger.MoneyPushed, $"������� ����� '{value:C}' �� ��������������"); 
            }

            if (container.CanPushMoney)
            {
                var change = Total + value - _drink.Amount;

                if (change > 0 && !_strategy.TryExecute(change, true))
                {
                    throw new MoneyOperationException(Trigger.LastBillReturned, "������ ����������. ��� ������ ������ ��� �����, ����������� ������ �������");
                }

                _bills.Push(value);
                container.Push();
            }
            else
            {
                throw new MoneyOperationException(Trigger.MoneyPushed, $"��������� ��� ����� ��������� '{value:C}' �����");
            }
        }

        public bool HasMoneyChange => Total > _drink.Amount;

        public bool IsAccountValid => Total >= _drink.Amount;

 
        public bool TryReturnMoneyChange(out decimal change)
        {
            change = Total - _drink.Amount;

            if (_strategy.TryExecute(change))
            {
                return true;
            }

            throw new MoneyOperationException(Trigger.ImpossibleToReturnMoneyDifference, $"���������� ������� �����");
        }

        public bool TryReturnAllMoney()
        {
            var bill = _bills.Peek();
            var container = _containers.FirstOrDefault(c => c.Denomination == bill);
            if (container == null || !container.CanPopMoney)
            {
                throw new MoneyOperationException(Trigger.SystemErrorOccurred, $"���������� ������� ������");
            }

            container.Pop();
            _bills.Pop();

            return true;
        }
    }
}