﻿using CoffeeMachine.Domain.Drinks;

namespace CoffeeMachine.Domain.Payments.ReturnMoneyStrategy
{
    public interface IReturnMoneyChangeStrategy
    {
        bool TryExecute(decimal moneyChange, bool forceRollback = false);
    }
}