﻿using System.Collections.Generic;
using System.Linq;
using CoffeeMachine.Domain.MonneyContainers;

namespace CoffeeMachine.Domain.Payments.ReturnMoneyStrategy
{
    public class SimpleMoneyChangeStrategy : IReturnMoneyChangeStrategy
    {
        private readonly IMonneyContainer[] _containers;

        public SimpleMoneyChangeStrategy(params IMonneyContainer[] containers)
        {
            _containers = containers;
        }

        public bool TryExecute(decimal moneyChange, bool forceRollback)
        {
            bool result = false;
            var operations = new List<ReturnOperation>();

            while (true)
            {
                var change = moneyChange;
                var container = _containers.Where(c => c.CanPopMoney && c.Denomination <= change)
                    .OrderByDescending(c => c.Denomination).FirstOrDefault();

                if (container == null)
                {
                    break;
                }

                var operation = new ReturnOperation(container);
                moneyChange = operation.Execute(moneyChange);
                operations.Add(operation);

                if (moneyChange <= 0)
                {
                    result = moneyChange == 0;
                    break;
                }
            }

            if (!result || forceRollback)
            {
                operations.ForEach(o => o.Rollback());
            }
            return result;
        }
    }

    internal class ReturnOperation
    {
        private readonly IMonneyContainer _container;

        public ReturnOperation(IMonneyContainer container)
        {
            _container = container;
        }

        public decimal Execute(decimal value)
        {
            _container.Pop();
            return value - _container.Denomination;
        }
        public void Rollback()
        {
            _container.Push();
        }
    }
}