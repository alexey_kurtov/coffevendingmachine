﻿using System.Collections.Generic;
using CoffeeMachine.Domain;
using CoffeeMachine.Domain.ComponentContainers;
using CoffeeMachine.Domain.Drinks;
using CoffeeMachine.Domain.MonneyContainers;
using CoffeeMachine.Domain.Payments;
using CoffeeMachine.Domain.Prices;
using CoffeeMachine.Web.Dto;
using CoffeeMachine.Web.Helper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CoffeeMachine.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors(o => o.AddPolicy("ApiPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            initCoffeeMachine(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("ApiPolicy");
            app.UseMvc();
        }


        private void initCoffeeMachine(IServiceCollection services)
        {
            var componentsContainer = new AllComponentContainer(
                //5 литров воды    
                new WaterContainer(5000),
                // 100 порций кофе
                new CoffeContainer(100),

                new AllCupTypesContainer(
                    // Тара 150 мл, 20 шт
                    new CupContainer(150, 20),
                    // Тара 200 мл, 10 шт
                    new CupContainer(200, 10),
                    // Тара 400 мл, 5 шт
                    new CupContainer(400, 5)
                )
            );

            var monneyContainers = new List<IMonneyContainer>
            {
                // 1 Руб, макс 200
                new StackMonneyContainer(1, 200),
                // 2 Руб, макс 100
                new StackMonneyContainer(2, 100),
                // 5 Руб, макс 50
                new StackMonneyContainer(5, 50),
                // 10 Руб, макс 50
                new StackMonneyContainer(10, 50),
                // 50 Руб, макс 10
                new StackMonneyContainer(50, 10),
                // 100 Руб, макс 10
                new StackMonneyContainer(100, 10),
            }.ToArray();

            var machine = new Domain.CoffeeMachine(new CoffeDrinkFacrory(new SimplePriceStrategy()), new AccountManagerFactory(monneyContainers), componentsContainer);

            services.AddSingleton<ICoffeeMachine>(p => machine);
            services.AddSingleton<IDrinkComponentContainer>(p => componentsContainer);
            services.AddSingleton(p => new MonneyStatisticHelper(monneyContainers));
        }
    }
}