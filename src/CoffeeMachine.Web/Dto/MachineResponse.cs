﻿using System;
using CoffeeMachine.Domain;

namespace CoffeeMachine.Web.Dto
{
    public class MachineResponse
    {
        public static MachineResponse Create(ICoffeeMachine machine)
        {
            return new MachineResponse
            {
                CurrenState = machine.GetCurrentState(),
                Messages = machine.GetLastMessages()
            };
        }

        public State CurrenState { get; set; }
        public String[] Messages { get; set; }
    }
}

