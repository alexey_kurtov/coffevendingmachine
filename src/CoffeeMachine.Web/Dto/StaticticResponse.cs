﻿using System;
using System.Text;

namespace CoffeeMachine.Web.Dto
{
    public class StaticticResponse
    {
        public static StaticticResponse Create(params string[] msgs)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var msg in msgs)
            {
                builder.AppendLine(msg);
            }

            return new StaticticResponse
            {
                CurrentState = builder.ToString()
            };
        }
        
        public String CurrentState { get; set; }
    }
}