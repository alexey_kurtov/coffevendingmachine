﻿using CoffeeMachine.Domain;
using CoffeeMachine.Domain.Drinks;
using CoffeeMachine.Web.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    public class DrinkTypeController : CoffeeMachineBaseController
    {

        public DrinkTypeController(ICoffeeMachine coffeeMachine) : base(coffeeMachine) { }

        [HttpGet("{type}")]
        public MachineResponse Get(DrinkType type)
        {
            CoffeeMachine.SelectDrink(type);
            return MachineResponse.Create(CoffeeMachine);
        }
    }
}