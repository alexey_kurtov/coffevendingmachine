﻿using CoffeeMachine.Domain;
using CoffeeMachine.Web.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    /// <summary>
    /// Отменить операцию
    /// </summary>
    
    public class CancelController : CoffeeMachineBaseController
    {

        public CancelController(ICoffeeMachine coffeeMachine) :base(coffeeMachine) { }

        [HttpGet]
        public MachineResponse Cancel()
        {
            CoffeeMachine.CancelOperation();
            return MachineResponse.Create(CoffeeMachine);
        }
    }
}