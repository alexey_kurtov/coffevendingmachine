﻿using CoffeeMachine.Domain;
using CoffeeMachine.Web.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;

namespace CoffeeMachine.Web.Controllers
{
    public class DrinkVolumeController : CoffeeMachineBaseController
    {

        public DrinkVolumeController(ICoffeeMachine coffeeMachine) : base(coffeeMachine) { }

        [HttpGet("{volume}")]
        public MachineResponse Get(int volume)
        {
            CoffeeMachine.SetDrinkVolume(volume);
            return MachineResponse.Create(CoffeeMachine);
        }
    }
}