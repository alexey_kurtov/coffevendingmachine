﻿using CoffeeMachine.Domain.ComponentContainers;
using CoffeeMachine.Web.Dto;
using CoffeeMachine.Web.Helper;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    [Route("api/[controller]")]
    public class StatisticController
    {
        private readonly IDrinkComponentContainer _componentContainer;
        private readonly MonneyStatisticHelper _monneyStatisticHelper;

        public StatisticController(IDrinkComponentContainer componentContainer, MonneyStatisticHelper monneyStatisticHelper)
        {
            _componentContainer = componentContainer;
            _monneyStatisticHelper = monneyStatisticHelper;
        }

        [HttpGet]
        public StaticticResponse GetStatistic()
        {
            return StaticticResponse.Create(_componentContainer.ToString(), _monneyStatisticHelper.GetStatisit());
        }
    }
}
