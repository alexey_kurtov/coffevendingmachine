﻿using CoffeeMachine.Domain;
using CoffeeMachine.Web.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    public class PushMoneyController : CoffeeMachineBaseController
    {
        public PushMoneyController(ICoffeeMachine coffeeMachine) : base(coffeeMachine) { }

        [HttpGet("{value}")]
        public MachineResponse Enter(decimal value)
        {
            CoffeeMachine.PushMoney(value);
            return MachineResponse.Create(CoffeeMachine);
        }
    }
}