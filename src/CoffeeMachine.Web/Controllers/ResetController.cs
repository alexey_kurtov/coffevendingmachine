﻿using CoffeeMachine.Domain;
using CoffeeMachine.Web.Dto;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    /// <summary>
    /// Инициализация исходного состояние
    /// </summary>
    public class ResetController : CoffeeMachineBaseController
    {
        public ResetController(ICoffeeMachine coffeeMachine) : base(coffeeMachine) { }

        [HttpGet]
        public MachineResponse Reset()
        {
            CoffeeMachine.Reset();
            return MachineResponse.Create(CoffeeMachine);
        }
    }
}