﻿using CoffeeMachine.Domain;
using Microsoft.AspNetCore.Mvc;

namespace CoffeeMachine.Web.Controllers
{
    [Route("api/[controller]")]
    public abstract class CoffeeMachineBaseController : Controller
    {
        protected readonly ICoffeeMachine CoffeeMachine;

        protected CoffeeMachineBaseController(ICoffeeMachine coffeeMachine)
        {
            CoffeeMachine = coffeeMachine;
        }
    }
}