﻿using System.Text;
using CoffeeMachine.Domain.MonneyContainers;

namespace CoffeeMachine.Web.Helper
{
    public class MonneyStatisticHelper
    {
        private readonly IMonneyContainer[] _containers;

        public MonneyStatisticHelper(params IMonneyContainer[] containers)
        {
            _containers = containers;
        }

        public string GetStatisit()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var container in _containers)
            {
                builder.AppendLine(container.ToString());
            }

            return builder.ToString();
        }
    }
}